/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.4.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform (0 0 0);

boundaryField
{
    inlet
    {
        type            codedFixedValue;
        value           uniform (0 0 0);
        redirectType    velocitySquareInlet;   

        code
        #{
            const fvPatch& boundaryPatch = patch(); 
            const vectorField& Cf = boundaryPatch.Cf(); 

            vectorField& field = *this; 

            scalar min = 0.501;  
            scalar max = 0.751; 

            forAll(Cf, faceI)
            {
               if (
                    (Cf[faceI].z() > min) &&
                    (Cf[faceI].z() < max) &&
                    (Cf[faceI].y() > min) &&
                    (Cf[faceI].y() < max) 
                  )
                {
                    field[faceI] = vector(1, 0, 0);
                }
            }
        #};
    }

    walls 
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }

    atmosphere
    {
        type            pressureInletOutletVelocity;
        value           uniform (0 0 0);
    }
}

// ************************************************************************* //
