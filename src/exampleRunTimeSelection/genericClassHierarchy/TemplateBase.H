/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2014 held by original authors 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::TemplateBase

Description
    Generic Base Class with RTS.      

SourceFiles
    TemplateBase.C

Authors:
    Tomislav Maric tomislav@sourceflux.de
    Jens Hoepken jens@sourceflux.de

\*---------------------------------------------------------------------------*/

#ifndef TemplateBase_H
#define TemplateBase_H

#include "dictionary.H"
#include "typeInfo.H"
#include "runTimeSelectionTables.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                         Class TemplateBase Declaration
\*---------------------------------------------------------------------------*/

template<class Parameter>
class TemplateBase
{

public:

    // Static data members
    TypeName("genericBase");

    declareRunTimeSelectionTable ( 	  	
        autoPtr,
        TemplateBase,
        Dictionary,
        (
            const dictionary& dict
        ),
        (dict)	
	) 		

    // Constructors
    TemplateBase();
        
    //- Construct from components
    explicit TemplateBase(const dictionary& dict);

    // Selectors
    static autoPtr<TemplateBase<Parameter> > New(
        const dictionary& dict
    );

    //- Destructor
    virtual ~TemplateBase() {};

    // Member functions
    virtual void execute() const;
};

//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define makeTemplateBase(Parameter)                                 \
                                                                    \
defineNamedTemplateTypeNameAndDebug(template##Parameter##Base, 0);  \
defineTemplateRunTimeSelectionTable(template##Parameter##Base, Dictionary); \
addToRunTimeSelectionTable                                          \
(                                                                   \
    template##Parameter##Base,                                      \
    template##Parameter##Base,                                      \
    Dictionary                                                      \
);

#define makeTemplateDerived(Parameter, Suffix)                      \
                                                                    \
defineNamedTemplateTypeNameAndDebug(template##Parameter##Suffix, 0);\
addToRunTimeSelectionTable                                          \
(                                                                   \
    template##Parameter##Base,                                      \
    template##Parameter##Suffix,                                    \
    Dictionary                                                      \
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#ifdef NoRepository
#   include "TemplateBase.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
