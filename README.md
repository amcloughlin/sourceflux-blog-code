Welcome to the [sourceflux](www.sourceflux.de) OpenFOAM Technology blog code repository

The repository contains both the simulation cases folder, as well as the source code for the solved exercises from the blog posts on our site. 

Supported OpenFOAM versions: 2.3.x and 2.4.x  

Have fun working through the examples!

Jens & Tomislav


# Compiling applications and running exercise simulation cases

1. Make sure that the OpenFOAM installation is properly configured. If it has been installed by a regular user in $HOME, then execute 

    source $WM_PROJECT_DIR/etc/bashrc

2. There are `Allwmake` and `Allclean` scripts prepared for automating the compilation of the training applications. Run `Allwmake` to compile the applications/solvers from the sourceflux OpenFOAM Technology Training slides. 


3. Every case usually contains 3 scripts: 
    1. `Allrun`
        - Runs `blockMesh` if necessary. 
        - Runs `setFields` if necessary to set the initial field values.
        - Runs the solver. 
    2. `Allvisualize`
        - If applicable: opens a pre-saved ParaView state file for the visualization in a setup used during the training course.  
        - If applicable: analyzes the reported post-processing data and generates a PDF diagram report that you can view in your browser. 

    3. `Allclean`
        - Cleans the simulation case
        - Removes all log files.  
        - Removes all time step directories.
