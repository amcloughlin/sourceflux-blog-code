/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2015- sourceflux 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    testChangingCellSet 

Description
    Constructs a cellSet, modifies it after each time-step increment, and
    stores it in an appropriate folder to allow for VTK conversion with
    `foamToVTK`. 

Authors
    Tomislav Maric tomislav@sourceflux.de

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "changingCellSet.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

int main(int argc, char *argv[])
{
    #include "setRootCase.H"
    #include "createTime.H"
    #include "createMesh.H"

    changingCellSet testCells(
        IOobject
        (
            "testCells", 
            runTime.timeName() + "/polyMesh/sets",
            runTime,  
            IOobject::NO_READ, 
            IOobject::AUTO_WRITE
        )
    ); 

    // Pre-process the cell set for the first time-step. 
    selectCircle(testCells, mesh);
    testCells.write(); 

    while(runTime.loop())
    {
        Info << "Time: " << runTime.timeName() << endl;
        selectCircle(testCells, mesh);
        
        // Loop over the keys of the cell set. 
        forAllIter(changingCellSet, testCells, cellIt)
        {
            Info << cellIt.key()  << " ";  
        }
        Info << endl;

        // Access n-th key. 5 used in this test. 
        const labelList& keys = testCells.toc(); 

        if (keys.size() < 5)
        {
            FatalErrorIn("testChangingCellSet::main line 80")
                << "Create a cell set containing at least 6 elements." << endl;
        }
        else 
            Info << "5th key = " << keys[5] << endl;

        runTime.write(); 
    }

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nEnd\n" << endl;
    return 0;
}


// ************************************************************************* //
